﻿namespace ParseSite.Models
{
    class ItemAvailability
    {
        public string id;
        public string shippingTime;
        public string content;
        public string showBasketButton;
    }
}
