﻿using log4net;
using log4net.Config;
using Microsoft.Win32;
using ParseSite.Engine;
using ParseSite.Models;
using ParseSite.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace ParseSite
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private BackgroundWorker _backgroundParser;
        private List<SingleItem> _lastResult;
        public event PropertyChangedEventHandler PropertyChanged;

        public CategoriesTreeViewModel _categoriesTreeViewModel;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public MainWindow()
        {
            XmlConfigurator.Configure();
            //MainCategories = new ObservableCollection<Category>(new List<Category> { new Category { Name = "Test", IsChecked = Visibility.Visible } });
            InitializeComponent();
            parseButton.Click += ParseButton_Click;
            saveButton.Click += SaveButton_Click;

            var categories = GetCategories();
            _categoriesTreeViewModel = new CategoriesTreeViewModel(categories);
            this.DataContext = _categoriesTreeViewModel;

            _backgroundParser = new BackgroundWorker
            {
                WorkerReportsProgress = true
            };
            _backgroundParser.ProgressChanged += _backgroundParser_ProgressChanged;
            _backgroundParser.DoWork += _backgroundParser_DoWork;
            _backgroundParser.RunWorkerCompleted += _backgroundParser_RunWorkerCompleted;
        }

        private List<Category> GetCategories()
        {
            Parser categoryParser = new Parser("http://sklep.olekmotocykle.pl/");
            return categoryParser.GetCategories();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog
            {
                FileName = "result.csv",
                Filter = "CSV (*.csv)|*.csv",
                Title = "Gdzie zapisać wynik?",
                InitialDirectory = Directory.GetCurrentDirectory()
            };

            if (saveDialog.ShowDialog(this) == true)
            {
                string csv = ToCsvModule.ToCsv(System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator, _lastResult);
                try
                {
                    using (var writer = new StreamWriter(saveDialog.FileName, false, Encoding.UTF8))
                    {
                        writer.Write(csv);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Problem z zapisem do pliku! Kliknij \"Zapisz dane\" aby spróbować jeszcze raz.", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void SaveResult(List<SingleItem> result)
        {
            saveButton.IsEnabled = true;
            _lastResult = result;
            saveButton.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private void _backgroundParser_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar.Value = 0;
            progressBar_category.Value = 0;
            SetLabelsVisible(false);
            SaveResult(e.Result as List<SingleItem>);
            parseButton.IsEnabled = true;
        }

        private void SetLabelsVisible(bool visible)
        {
            var visibility = visible ? Visibility.Visible : Visibility.Hidden;
            labelCat.Visibility = visibility;
            labelCatCurr.Visibility = visibility;
            labelCount.Visibility = visibility;
            labelCountCurr.Visibility = visibility;
        }

        private void _backgroundParser_DoWork(object sender, DoWorkEventArgs e)
        {
            var start = DateTime.Now;
            Console.WriteLine("Process start: " + start);
            BackgroundWorker worker = sender as BackgroundWorker;
            Parser mainParser = new Parser("http://sklep.olekmotocykle.pl/opony.html");
            mainParser.ProgressChanged += (s, pe) => worker.ReportProgress(pe.ProgressPercentage, pe.UserState);
            mainParser.CategoryProgressChanged += (s, pe) => _backgroundParser_CategoryProgressChanged(this, pe);
            mainParser.ResetProgress += (s, pe) => _backgroundParser_ResetProgress(this, pe);
            try
            {
                List<CategoryViewModel> catList = GetCategoriesToParse(_categoriesTreeViewModel.RootCategories);
                //e.Result = mainParser.Parse();
                e.Result = mainParser.ParseList(catList);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
            }

            var elapsed = DateTime.Now - start;
            Console.WriteLine("Process start: " + DateTime.Now);
            Console.WriteLine("Process time elapsed: " + elapsed);
        }

        private List<CategoryViewModel> GetCategoriesToParse(List<CategoryViewModel> catNode)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            
            foreach (var cat in catNode)
            {
                if (cat.Children?.Count > 0)
                {
                    result.AddRange(GetCategoriesToParse(cat.Children));
                }
                else if (cat.IsChecked)
                {
                    result.Add(cat);
                }
            }

            return result;
        }

        private void _backgroundParser_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Maximum = Convert.ToInt32(e.UserState);
            progressBar.Value += e.ProgressPercentage;
            labelCountCurr.Content = progressBar.Value + "/" + e.UserState;
        }
    
        private void _backgroundParser_CategoryProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => ChangeCategoryValues(e));
            }
            else
            {
                ChangeCategoryValues(e);
            }
        }

        private void _backgroundParser_ResetProgress(object sender, ProgressChangedEventArgs e)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => ResetProgress((int)e.UserState));
            }
            else
            {
                ResetProgress((int)e.UserState);
            }
        }

        private void ChangeCategoryValues(ProgressChangedEventArgs e)
        {
            CategoryProgress userState = e.UserState as CategoryProgress;
            progressBar_category.Maximum = userState.MaxProgress;
            progressBar_category.Value = e.ProgressPercentage;
            labelCatCurr.Content = userState.CatName + " - " + userState.CurrentProgress + "/" + userState.MaxProgress;
        }

        private void ResetProgress(int max)
        {
            progressBar.Maximum = max;
            progressBar.Value = 0;
            labelCountCurr.Content = progressBar.Value + "/" + max;
        }

        private void ParseButton_Click(object sender, RoutedEventArgs e)
        {
            parseButton.IsEnabled = false;
            SetLabelsVisible(true);
            _backgroundParser.RunWorkerAsync();
        }
    }
}
