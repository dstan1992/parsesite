﻿using HtmlAgilityPack;
using log4net;
using ParseSite.Models;
using ParseSite.ViewModels;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ParseSite.Engine
{
    public class Parser
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static int testCount = 0;
        private string _url;
        private string _domain;
        public event ProgressChangedEventHandler ProgressChanged;
        public event ProgressChangedEventHandler CategoryProgressChanged;
        public event ProgressChangedEventHandler ResetProgress;

        public Parser(string url)
        {
            _url = url;
            _domain = _url.Substring(0, _url.LastIndexOf('/'));
        }

        public List<SingleItem> ParseList(List<CategoryViewModel> catList)
        {
            var start = DateTime.Now;
            List<SingleItem> result = new List<SingleItem>();
            List<Category> categories = new List<Category>();
            using (WebClient client = GetWebClientWithEncoding())
            {
                string htmlCode = client.DownloadString(_url);
                categories = catList.Select(x => x.GetCategory()).ToList();
            }
            int counter = 0;
            foreach (var category in categories)
            {
                OnCategoryProgressChanged(counter++, categories.Count, category);
                result.AddRange(ParseCategory(category).Result);
            }
            var elapsed = DateTime.Now - start;
            return result;
        }

        private async Task<List<SingleItem>> ParseCategory(Category category)
        {
            ConcurrentBag<SingleItem> result = new ConcurrentBag<SingleItem>();
            List<Task> tasksList = new List<Task>();
            using (WebClient client = GetWebClientWithEncoding())
            {
                client.Encoding = Encoding.UTF8;
                client.Proxy = null;
                string htmlCode = client.DownloadString(category.Link);

                int pageCount = GetPageCount(htmlCode);
                OnResetProgress(pageCount);
                List<string> sites;

                if (pageCount > 1)
                {
                    string paginationAddress = GetPaginationAddress(htmlCode);
                    sites = GetSitesList(paginationAddress, pageCount);
                }
                else
                {
                    sites = new List<string> { category.Link };
                }

                foreach (var site in sites)
                {
                    var task = Task.Factory.StartNew(() =>
                    {
                        var singlePageResult = ParseSinglePage(site);
                        if (singlePageResult != null)
                        {
                            result.AddRange(singlePageResult);
                        }
                    }).ContinueWith((endedTask) =>
                    {
                        OnProgressChanged(1, sites.Count);
                    });
                    tasksList.Add(task);
                }
            }
            await Task.WhenAll(tasksList);
            return result.ToList();
        }

        private WebClient GetWebClientWithEncoding()
        {
            WebClient result = new WebClient
            {
                Encoding = Encoding.UTF8,
                Proxy = null
            };
            return result;
        }

        private List<string> GetSitesList(string paginationAddress, int count)
        {
            List<string> result = new List<string>();
            for (int i = 1; i <= count; ++i)
            {
                result.Add(string.Format(paginationAddress, i));
            }
            return result;
        }

        private string GetPaginationAddress(string htmlCode)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlCode);
            var paginationUrl = doc.DocumentNode.SelectSingleNode("//li[contains(@class, \"pagList-next\")]/a").Attributes.FirstOrDefault(att => att.Name == "href").Value;
            return _domain + paginationUrl.Substring(0, paginationUrl.LastIndexOf('/') + 1) + "{0}";
        }

        private int GetPageCount(string htmlCode)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlCode);
            string paginationUrl = doc.DocumentNode.SelectSingleNode("//ul[@class=\"pagList\"]/li[last()-2]/a")?.Attributes.FirstOrDefault(att => att.Name == "href").Value;
            return paginationUrl != null ? 
                int.Parse(paginationUrl.Substring(paginationUrl.LastIndexOf('/') + 1)) :
                1;
        }

        private IEnumerable<SingleItem> ParseSinglePage(string url)
        {
            int taskNum = testCount++;
            Console.WriteLine("Task " + taskNum + " start at " + DateTime.Now.ToString());
            var doc = new HtmlDocument();
            using (var client = GetWebClientWithEncoding())
            {
                try
                {
                    var htmlCode = client.DownloadString(url);
                    doc.LoadHtml(htmlCode);
                }
                catch (Exception ex)
                {
                    _log.Error("Error during parsing page " + url + ". Error message:\n" + ex.Message);
                }
            }

            var itemNodes = doc.DocumentNode.SelectNodes("//td[contains(@class, \"productList-widthOneProd\")]");
            if (itemNodes == null || itemNodes.Count <= 0)
            {
                return null;
            }

            var result = itemNodes?.Select(x => new SingleItem
            {
                Symbol = GetInnerTextXPath(x, ".//td[contains(@class, \"product-attrSymbolCol\")]"),
                Name = GetInnerTextXPath(x, ".//h2[contains(@class, \"productList-prodTitle\")]/a"),
                Producer = GetInnerTextXPath(x, ".//p[@class=\"product-producent\"]/strong"),
                //Avail = GetInnerTextXPath(x, ".//span[contains(@class, \"product-attrAvNumber\")]"),
                PriceB = GetInnerTextXPath(x, ".//span[contains(@class, \"brutto\")]"),
                PriceN = GetInnerTextXPath(x, ".//span[contains(@class, \"netto\")]"),
                PhotoLink = GetAttributeXPath(x, ".//div[contains(@class, \"product-images\")]/a/img", "src").Replace("160x160/", ""),
                PaginationAddress = url
            }).ToList();

            var availDict = GetAvailForItems(result);
            foreach (var avail in availDict)
            {
                result.FirstOrDefault(x => x.Symbol.Equals(avail.Key)).Avail = avail.Value;
            }

            Console.WriteLine("Task " + taskNum + " end at " + DateTime.Now.ToString());
            return result;
        }

        private Dictionary<string, string> GetAvailForItems(IEnumerable<SingleItem> items)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if (items.Count() <= 0)
            {
                return result;
            }

            try
            {
                HttpHelper helper = new HttpHelper("http://sklep.olekmotocykle.pl/pl-PL/product/ajax/getproductinstancesdepositoryhtmldata");
                var values = helper.GetDataForParams(items.Where(item => !string.IsNullOrEmpty(item.Symbol)).Select(item => item.Symbol)).Result;

                foreach (var singleValue in values)
                {
                    var doc = new HtmlDocument();
                    doc.LoadHtml(singleValue.Value);

                    var avail = GetInnerTextXPath(doc.DocumentNode, ".//span[contains(@class, \"product-attrAvNumber\")]");
                    result.Add(singleValue.Key, avail);
                }
                return result;
            }
            catch (Exception ex)
            {
                _log.Error("Error during fetching availability for items from page " + items.First().PaginationAddress + ". Error message:\n" + ex.Message);
            }

            return result;
        }

        private string GetInnerText(HtmlNode node, string nodeClass)
        {
            return node.Descendants().FirstOrDefault(descendant => descendant.HasClass(nodeClass))?.InnerHtml.Trim();
        }

        private string GetInnerTextStrong(HtmlNode node, string nodeClass)
        {
            return node.Descendants().FirstOrDefault(descendant => descendant.HasClass(nodeClass)).SelectSingleNode("strong")?.InnerHtml.Trim();
        }

        private string GetInnerTextXPath(HtmlNode node, string xPath)
        {
            return node.SelectSingleNode(xPath)?.InnerHtml.Trim().Trim(new char [] { '(', ')' }) ?? string.Empty;
        }

        private string GetAttributeXPath(HtmlNode node, string xPath, string attName)
        {
            return node.SelectSingleNode(xPath).GetAttributeValue(attName, null);
        }

        public List<Category> GetCategories()
        {
            using (WebClient client = GetWebClientWithEncoding())
            {
                string htmlCode = client.DownloadString(_url);
                return GetCategories(htmlCode);
            }
        }

        private List<Category> GetCategories(string htmlCode)
        {
            List<Category> result = new List<Category>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlCode);

            var nodes = doc.DocumentNode.SelectNodes("//div[@class='mainMenu-category']//a[@class='mainMenu-categoryLink']");
            result.AddRange(nodes.Select(node => new Category
            {
                Name = GetCategoryName(node),
                Link = node.Attributes["href"].Value,
                IsChecked = true
            }));
            result.ForEach(category => GetSubCategoriesAndCount(category, doc));

            return result;
        }

        private void GetSubCategoriesAndCount(Category category, HtmlDocument doc)
        {
            List<Category> result = new List<Category>();

            var nodes = doc.DocumentNode.SelectNodes("//div[@class='mainMenu-category']//a[@class='mainMenu-categoryLink' and @href='" + category.Link + "']/parent::div");
            foreach (var liNode in nodes.Descendants("li"))
            {
                var childCategory = new Category
                {
                    Name = liNode.FirstChild.InnerHtml,
                    Link = liNode.FirstChild.Attributes["href"].Value,
                    IsChecked = true
                };
                category.Children.Add(childCategory);
            }
        }

        private string GetCategoryName(HtmlNode node)
        {
            int countStart = node.InnerHtml.IndexOf("(");
            if (countStart < 0)
            {
                return node.InnerText;
            }
            else
            {
                return node.InnerText.Substring(0, countStart - 1);
            }
        }

        private int? GetCategoryCount(HtmlNode node)
        {
            int countStart = node.InnerText.IndexOf("(");
            int countEnd = node.InnerText.IndexOf(")");
            if (countStart < 0 || countEnd < 0)
            {
                return null;
            }
            return int.Parse(node.InnerText.Substring(countStart + 1, countEnd - countStart - 1));
        }

        protected virtual void OnProgressChanged(int progress, int max)
        {
            ProgressChanged?.Invoke(this, new ProgressChangedEventArgs(progress, max));
        }

        protected virtual void OnCategoryProgressChanged(int progress, int max, Category cat)
        {
            CategoryProgressChanged?.Invoke(this, new ProgressChangedEventArgs(progress, new CategoryProgress
            {
                CatName = cat.Name,
                CurrentProgress = progress,
                MaxProgress = max
            }));
        }

        protected virtual void OnResetProgress(int max)
        {
            ResetProgress?.Invoke(this, new ProgressChangedEventArgs(0, max));
        }
    }
}
