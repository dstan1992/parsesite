﻿using HtmlAgilityPack;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseSite
{
    public static class ExtensionMethods
    {
        public static string GetInnerTextXPath(this HtmlNode node, string nodeClass)
        {
            return node.Descendants().FirstOrDefault(descendant => descendant.HasClass(nodeClass))?.InnerHtml.Trim();
        }

        public static void AddRange<T>(this ConcurrentBag<T> @this, IEnumerable<T> toAdd)
        {
            if (toAdd == null || toAdd.Count() <= 0)
            {
                return;
            }

            foreach (var element in toAdd)
            {
                @this.Add(element);
            }
        }

        public static string EscapeCsv(this string str)
        {
            string result = str;
            if (str.Contains(";"))
            {
                result = "\"" + str + "\"";
            }
            if (str.Contains("\""))
            {
                result = result.Replace("\"", "\"\"");
            }
            return result;
        }
    }
}
