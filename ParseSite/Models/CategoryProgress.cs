﻿namespace ParseSite.Models
{
    public class CategoryProgress
    {
        public int CurrentProgress;
        public int MaxProgress;
        public string CatName;
    }
}
