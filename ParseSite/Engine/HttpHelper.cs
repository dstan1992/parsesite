﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using ParseSite.Models;

namespace ParseSite.Engine
{
    public class HttpHelper
    {
        private string _url;
        public HttpHelper(string url)
        {
            _url = url;
        }

        public async Task<Dictionary<string, string>> GetDataForParams(IEnumerable<string> ids)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            int counter = 0;
            foreach (var id in ids)
            {
                parameters.Add("params[" + counter++ + "]", id);
            }
            var encodedContent = new FormUrlEncodedContent(parameters);
            using (var client = new HttpClient())
            {
                var response = await client.PostAsync(_url, encodedContent);
                var responseString = response.Content.ReadAsStringAsync().Result;
                var jsonResponseContent = JsonConvert.DeserializeObject<List<ItemAvailability>>(responseString);
                jsonResponseContent.ForEach(json => result.Add(json.id, json.content));
            }

            return result;
        }
    }
}
