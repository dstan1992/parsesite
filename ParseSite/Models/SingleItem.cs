﻿namespace ParseSite.Models
{
    public class SingleItem
    {
        public string Symbol;
        public string Avail;
        public string Name;
        public string Producer;
        public string PriceB;
        public string PriceN;
        public string PhotoLink;
        public string PaginationAddress;
    }
}
