﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace ParseSite.Models
{
    public class Category
    {
        public string Name;
        public string Link;
        public int Count;

        private List<Category> _children = new List<Category>();
        public IList<Category> Children
        {
            get
            {
                return _children;
            }
        }
        
        public bool IsChecked;
    }
}
