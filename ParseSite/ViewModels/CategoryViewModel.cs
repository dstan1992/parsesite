﻿using ParseSite.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace ParseSite.ViewModels
{
    public class CategoryViewModel : INotifyPropertyChanged
    {
        private List<CategoryViewModel> _children;
        private Category _category;
        private CategoryViewModel _parent;

        public event PropertyChangedEventHandler PropertyChanged;

        public CategoryViewModel(Category category)
            : this(category, null)
        {
        }

        private CategoryViewModel(Category category, CategoryViewModel parent)
        {
            _category = category;
            _parent = parent;
            IsChecked = category.IsChecked;

            _children = new List<CategoryViewModel>(_category.Children.Select(child => new CategoryViewModel(child, this)));
        }

        public Category GetCategory()
        {
            return _category;
        }

        private bool _isChecked;
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                if (value != _isChecked)
                {
                    _isChecked = value;
                    RaisePropertyChanged();
                }

                // Expand all the way up to the root.
                if (_isChecked && _parent?.IsChecked == false)
                    _parent.IsChecked = true;

                if (!_isChecked && _parent != null && !_parent.Children.Any(child => child.IsChecked))
                {
                    _parent.IsChecked = false;
                }

                if (Children?.Count > 0)
                {
                    if (value && Children.Any(child => child.IsChecked == value))
                    {
                        return;
                    }
                    // Check/Uncheck all children
                    foreach (var child in Children.Where(child => child.IsChecked != value))
                    {
                        child.IsChecked = value;
                    }
                }
            }
        }

        public string Name
        {
            get
            {
                return _category.Name;
            }
        }

        public List<CategoryViewModel> Children
        {
            get
            {
                return _children;
            }
        }

        private void RaisePropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
