﻿using ParseSite.Models;
using System.Linq;
using System.Collections.Generic;

namespace ParseSite.ViewModels
{
    public class CategoriesTreeViewModel
    {
        private List<CategoryViewModel> _rootCategories;
        public CategoriesTreeViewModel(List<Category> rootCategories)
        {
            _rootCategories = new List<CategoryViewModel>(rootCategories.Select(category => new CategoryViewModel(category)));
        }

        public List<CategoryViewModel> RootCategories
        {
            get
            {
                return _rootCategories;
            }
        }

    }
}
